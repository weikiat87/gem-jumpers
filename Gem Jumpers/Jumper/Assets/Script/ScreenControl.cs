﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class MovementEvent : UnityEvent<Movement>
{
	
}

public class ScreenControl : MonoBehaviour, IOnPressedHandler, IPointerUpHandler, IPointerDownHandler
{
	[SerializeField] private Text m_debugText;
	[SerializeField] private MovementEvent m_pressedEvent;
	private Vector2 m_clickPos;

	#region IPointerDownHandler implementation

	public void OnPointerDown (PointerEventData eventData)
	{
		m_clickPos = eventData.position;
	}

	#endregion

	#region IOnPressedHandler implementation

	public void OnPressed (PointerEventData eventData)
	{
		if(eventData.position.x/Screen.width > 0.5f)
		{
			m_pressedEvent.Invoke(Movement.right);
		}
		else
		{
			m_pressedEvent.Invoke(Movement.left);
		}
	}

	#endregion
	
	#region IPointerUpHandler implementation

	public void OnPointerUp (PointerEventData eventData)
	{
		m_debugText.text = eventData.clickCount.ToString();
		if(eventData.clickCount > 1)
		{
			m_pressedEvent.Invoke(Movement.jump);
		}
		else if(Vector2.Angle((eventData.position - m_clickPos),Vector2.up) < 60.0f && (eventData.position - m_clickPos).magnitude > 5.0f)
		{
			m_pressedEvent.Invoke(Movement.jump);
		}
	}
	#endregion



}
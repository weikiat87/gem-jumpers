﻿using UnityEngine;
using System.Collections;

public class Section : ScriptableObject
{
	[SerializeField] private Platform[] m_platformsList;
	[SerializeField] private int		m_defaultSectionHeight;

	public int SectionHeight
	{
		get { return m_defaultSectionHeight; }
	}

	public static int currentHeight = -4;

	public GameObject Create()
	{
		currentHeight++;
		return	GeneratePlatforms(m_defaultSectionHeight);
	}

	public GameObject Create(int _height)
	{
		currentHeight++;
		return GeneratePlatforms(_height);
	}


	private GameObject GeneratePlatforms(int _height)
	{
		GameObject go = new GameObject();
		go.name = "Section" + currentHeight;

		for(int i=0;i<_height;i++)
		{
			Platform currentPlatform = Instantiate(m_platformsList[Random.Range(0,m_platformsList.Length)]);
			Vector3 platformPosition = new Vector3(Random.Range(-5,5),i+currentHeight,0);
			currentPlatform.transform.localPosition = platformPosition;
			currentPlatform.transform.SetParent(go.transform);
		}

		currentHeight += _height;

		return go;
	}
}
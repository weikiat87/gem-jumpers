﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;



public class CustomTouchInputModule : TouchInputModule
{

	public override void Process ()
	{
		if(forceModuleActive)
		{
			FakeTouches();
		}
		else
		{
			ProcessTouchEvents();
		}

	}
	protected void ProcessTouchEvents()
	{
		for (int i = 0; i < Input.touchCount; i++)
		{
			Touch touch = Input.GetTouch (i);
			bool pressed;
			bool flag;
			PointerEventData touchPointerEventData = base.GetTouchPointerEventData (touch, out pressed, out flag);
			this.ProcessTouchPress (touchPointerEventData, pressed, flag);
			if (!flag)
			{
				this.ProcessMove (touchPointerEventData);
				this.ProcessDrag (touchPointerEventData);
			}
			else
			{
				base.RemovePointerData (touchPointerEventData);
			}
		}
	}

	
	protected void ProcessTouchPress (PointerEventData pointerEvent, bool pressed, bool released)
	{
		GameObject gameObject = pointerEvent.pointerCurrentRaycast.gameObject;
		if (pressed)
		{
			pointerEvent.eligibleForClick = true;
			pointerEvent.delta = Vector2.zero;
			pointerEvent.dragging = false;
			pointerEvent.useDragThreshold = true;
			pointerEvent.pressPosition = pointerEvent.position;
			pointerEvent.pointerPressRaycast = pointerEvent.pointerCurrentRaycast;
			base.DeselectIfSelectionChanged (gameObject, pointerEvent);
			if (pointerEvent.pointerEnter != gameObject)
			{
				base.HandlePointerExitAndEnter (pointerEvent, gameObject);
				pointerEvent.pointerEnter = gameObject;
			}
			GameObject gameObject2 = ExecuteEvents.ExecuteHierarchy<IPointerDownHandler> (gameObject, pointerEvent, ExecuteEvents.pointerDownHandler);
			if (gameObject2 == null)
			{
				gameObject2 = ExecuteEvents.GetEventHandler<IPointerClickHandler> (gameObject);
			}
			float unscaledTime = Time.unscaledTime;
			if (gameObject2 == pointerEvent.lastPress)
			{
				float num = unscaledTime - pointerEvent.clickTime;
				if (num < 0.3f)
				{
					pointerEvent.clickCount++;
				}
				else
				{
					pointerEvent.clickCount = 1;
				}
				pointerEvent.clickTime = unscaledTime;
			}
			else
			{
				pointerEvent.clickCount = 1;
			}
			pointerEvent.pointerPress = gameObject2;
			pointerEvent.rawPointerPress = gameObject;
			pointerEvent.clickTime = unscaledTime;
			pointerEvent.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler> (gameObject);
			if (pointerEvent.pointerDrag != null)
			{
				ExecuteEvents.Execute<IInitializePotentialDragHandler> (pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.initializePotentialDrag);
			}
		}
		if (released)
		{
			ExecuteEvents.Execute<IPointerUpHandler> (pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerUpHandler);
			GameObject eventHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler> (gameObject);
			if (pointerEvent.pointerPress == eventHandler && pointerEvent.eligibleForClick)
			{
				ExecuteEvents.Execute<IPointerClickHandler> (pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerClickHandler);
			}
			else
			{
				if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
				{
					ExecuteEvents.ExecuteHierarchy<IDropHandler> (gameObject, pointerEvent, ExecuteEvents.dropHandler);
				}
			}
			pointerEvent.eligibleForClick = false;
			pointerEvent.pointerPress = null;
			pointerEvent.rawPointerPress = null;
			if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
			{
				ExecuteEvents.Execute<IEndDragHandler> (pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);
			}
			pointerEvent.dragging = false;
			pointerEvent.pointerDrag = null;
			if (pointerEvent.pointerDrag != null)
			{
				ExecuteEvents.Execute<IEndDragHandler> (pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);
			}
			pointerEvent.pointerDrag = null;
			ExecuteEvents.ExecuteHierarchy<IPointerExitHandler> (pointerEvent.pointerEnter, pointerEvent, ExecuteEvents.pointerExitHandler);
			pointerEvent.pointerEnter = null;
		}
	}


	protected void FakeTouches()
	{
		PointerInputModule.MouseState mousePointerEventData = this.GetMousePointerEventData (0);
		PointerInputModule.MouseButtonEventData eventData = mousePointerEventData.GetButtonState (PointerEventData.InputButton.Left).eventData;
		if (eventData.PressedThisFrame ())
		{
			eventData.buttonData.delta = Vector2.zero;
		}
		this.ProcessTouchPress (eventData.buttonData, eventData.PressedThisFrame (), eventData.ReleasedThisFrame ());

		if (Input.GetMouseButton (0))
		{
			this.ProcessMove (eventData.buttonData);
			this.ProcessDrag (eventData.buttonData);
			this.ProcessPressed (eventData.buttonData);
			
		}
	}

	private void ProcessPressed(PointerEventData eventData)
	{
		ExecuteEvents.Execute<IOnPressedHandler>(eventData.pointerPressRaycast.gameObject,eventData,PressModuleExecute.pressHandler);
	}
}

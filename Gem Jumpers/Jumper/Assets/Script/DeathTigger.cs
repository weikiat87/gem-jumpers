﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DeathTigger : MonoBehaviour 
{
	[SerializeField] private float speedModifier;
	[SerializeField] private float timeModier;

	

	private void Update()
	{
		speedModifier = Section.currentHeight*0.02f;
		transform.Translate(Vector3.up*Time.deltaTime*speedModifier );
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if(other.name == "Player") SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
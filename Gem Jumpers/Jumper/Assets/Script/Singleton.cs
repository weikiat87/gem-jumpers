﻿using UnityEngine;

/// <summary>
/// Base for Singleton Managers (Where T is a type of MonoBehaviour)
/// </summary>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T instance;
	protected static object lockObj = new object();

	[SerializeField] protected bool dontDestroy;

	/// <summary>
	/// Get Singleton static instance.
	/// </summary>
	/// <value>instance</value>
	public static T Instance
	{
		get
		{
			if (applicationIsQuitting) {
				Debug.LogWarning("[Singleton] Instance '"+ typeof(T) +
				                 "' already destroyed on application quit." +
				                 " Won't create again - returning null.");
				return null;
			}
			
			lock(lockObj)
			{
				if (instance == null)
				{
					instance = (T) FindObjectOfType(typeof(T));
					
					if ( FindObjectsOfType(typeof(T)).Length > 1 )
					{
						Debug.LogError("[Singleton] Something went really wrong " +
						               " - there should never be more than 1 singleton!" +
						               " Please remove unwanted Instances.");
						return instance;
					}
				}
				return instance;
			}	
		}
	}


	/// <summary>
	/// Default Awake for Singleton Classes.
	/// </summary>
	protected virtual void Awake()
	{
		if(instance == null)						instance = (T)this.GetComponent<T>();		// for being a lazy person
		if(instance != (T)this.GetComponent<T>())	Destroy(this.gameObject);					// destroy game object

		if(dontDestroy)								DontDestroyOnLoad(this.gameObject);			// for special cases
	}

	
	private static bool applicationIsQuitting = false;
	/// <summary>
	/// When Unity quits, it destroys objects in a random order.
	/// In principle, a Singleton is only destroyed when application quits.
	/// If any script calls Instance after it have been destroyed, 
	///   it will create a buggy ghost object that will stay on the Editor scene
	///   even after stopping playing the Application. Really bad!
	/// So, this was made to be sure we're not creating that buggy ghost object.
	/// </summary>
	public void OnDestroy() 
	{
		// removed for testing
		applicationIsQuitting = true;

		// clear static variable
		instance = null;
	}

	protected void OnEnable()
	{
		applicationIsQuitting = false;
	}
}
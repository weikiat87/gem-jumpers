﻿using UnityEngine;
using System.Collections;

public class PlatformManager : Singleton<PlatformManager>
{
	[SerializeField] private Section[] m_sectionList;


	[SerializeField]  private GameObject m_currentSection;
	[SerializeField]  private GameObject m_nextSection;



	private void Start()
	{
		Section.currentHeight = -4;
		m_currentSection = GetSection();
		m_nextSection	 = GetSection();
	}

	public void GenerateNextSection()
	{
		m_currentSection = m_nextSection;
		m_nextSection = GetSection();
	}

	private GameObject GetSection()
	{
		return m_sectionList[Random.Range(0,m_sectionList.Length)].Create();
	}
}
﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public GameObject m_followTarget;
	[SerializeField] private float m_checkDistance;
	[SerializeField] private Axis m_axis;

	private void Start()
	{
		switch(m_axis)
		{
		case Axis.x: m_checkDistance = transform.position.x; break;
		case Axis.y: m_checkDistance = transform.position.y; break;
		case Axis.z: m_checkDistance = transform.position.z; break;
		}
	}

	private void Update()
	{
		if(m_followTarget == null) return;

		if(m_followTarget.transform.position.y > m_checkDistance)
		{
			transform.position = transform.position.WithY(m_followTarget.transform.position.y);
		}
		else
		{
			transform.position = transform.position.WithY(m_checkDistance);
		}
	}
}
﻿using UnityEngine;
using System.Collections;


namespace UnityEngine.EventSystems
{
	
	public interface IOnPressedHandler : IEventSystemHandler 
	{
		void OnPressed(PointerEventData eventData);
	}

	
	public static class PressModuleExecute
	{
		private static void Execute (IOnPressedHandler handler, BaseEventData eventData)
		{
			handler.OnPressed (ExecuteEvents.ValidateEventData<PointerEventData> (eventData));
		}
		
		public static ExecuteEvents.EventFunction<IOnPressedHandler> pressHandler 
		{
			get { return Execute; }
		}
	}
}

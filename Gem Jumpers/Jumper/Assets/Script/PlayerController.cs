﻿using UnityEngine.EventSystems;
using UnityEngine;
using System.Collections;

public enum Movement { left, right, jump };

public class PlayerController : MonoBehaviour
{
	[SerializeField] private float m_speed;
	[SerializeField] private float m_jump;
	[SerializeField] private float m_limit;
	[SerializeField] private float m_speedLimit;
	private enum PlayerState { idle,jump }

	private PlayerState	 	m_currentPlayerState;
	private Rigidbody2D 	m_rigidBody;
	private BoxCollider2D	m_collider;
	private int				previousHeight;

	private void Start()
	{
		m_rigidBody		= GetComponent<Rigidbody2D>();
		m_collider  	= GetComponent<BoxCollider2D>();
	}

	private void Update()
	{
		if(transform.position.x > m_limit)			transform.position = new Vector2(-m_limit,transform.position.y);
		else if(transform.position.x < -m_limit)	transform.position = new Vector2(m_limit,transform.position.y);

		if(m_rigidBody.velocity.y<0)
		{
			m_collider.enabled = true;
		}

		if(transform.position.y > Section.currentHeight/2)
		{
			previousHeight = Section.currentHeight;
			PlatformManager.Instance.GenerateNextSection();
		}
	}

	public void Move(Movement movement)
	{
		switch(movement)
		{
		case Movement.left:		m_rigidBody.velocity = m_rigidBody.velocity.WithX(-m_speed); break;
		case Movement.right:	m_rigidBody.velocity = m_rigidBody.velocity.WithX(m_speed); break;
		case Movement.jump:		
			if(m_currentPlayerState != PlayerState.jump)
			{
				m_collider.enabled = false;
				m_rigidBody.AddForce(Vector2.up * m_jump); 
				m_currentPlayerState = PlayerState.jump;
			}
			break;
		}

	}

	private void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.CompareTag("Platform"))
		{
			m_currentPlayerState = PlayerState.idle;
		}

	}

	public void Reset()
	{
		Debug.Log("Resetting level..");
		Application.LoadLevel(Application.loadedLevel);
	}
}
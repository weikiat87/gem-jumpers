﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Section))]
public class SectionEditor : Editor
{

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
	}

	[MenuItem("Assets/Create/New Section")]
	private static void Create()
	{
		// Create and add a new ScriptableObject for storing configuration
		ScriptableObjectUtility.CreateAsset<Section>();
	}

}